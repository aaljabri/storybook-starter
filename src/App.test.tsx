import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders storybook", () => {
  render(<App />);
  const storybook = screen.getByText(/storybook/i);
  expect(storybook).toBeInTheDocument();
});
